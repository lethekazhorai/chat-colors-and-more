# Chat Colors & More

**This project is not maintained as I am not using (and cannot use) Foundry VTT for the time being. Please do not file any issues or make feature requests as I cannot be of assistance. Instead, feel free to fork the project and make your own improvements (you can relicense it.)**

![Screenshot](https://gitlab.com/lethekazhorai/chat-colors-and-more/-/raw/master/ChatColors_Example.PNG?inline=true "Screenshot")

A small module for [Foundry Virtual Tabletop](http://foundryvtt.com) created for my personal use.

It allows players to set custom colors for different types of chat messages (IC speech, emotes, rolls, and all other messages) in the Module Settings. 

* A GM must grant the player permission to "Configure Module Settings" for them to be able to access these settings.
* After changing any colors, players must log and back in for their chatlog to update.
* It isn't possible to specify unique message heading colors. (This wouldn't be difficult, but I didn't want to add even more setting fields.)

The module also has the following extra feature:

* Players can specify a default chat prefix,  which will be prefixed to all their chat messages that are not already commands. This can be used to, for example, make chat messages default to OOC speech (by using "/ooc" as a prefix) even if a GM or player has a token selected.

## Changelog

* **0.0.2**: Made a minor internal change that may improve compatibility with other modules.
* **0.0.1**: Initial release.

## Requirements

This module was written and tested on Foundry VTT 0.5.3. It may work on older versions (if the manifest is edited) but I can't promise that it will.

## Installation Instructions

To install, follow these instructions:

1.  Inside Foundry, select the Add-on Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: https://gitlab.com/lethekazhorai/chat-colors-and-more/-/raw/master/module.json
3.  Click Install and wait for installation to complete.

## License
<a rel="license" href="https://creativecommons.org/share-your-work/public-domain/cc0/"><img alt="CC0 License" style="border-width:0" src="https://licensebuttons.net/p/zero/1.0/88x31.png" /></a><br />This work is licensed under the <a rel="license" href="https://creativecommons.org/share-your-work/public-domain/cc0/">CC0 License</a>.